<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;

Route::get('/', function () {
    return redirect()->route('presupuestos.listado');
});

Route::middleware('auth')
    ->namespace('Admin\\')
    ->group(function () {

//        Route::get('refresh-csrf', function(){
//            session()->regenerate();
//            return csrf_token();
//        })->name('refresh_token');

        //USUARIOS
        Route::get('usuarios', 'UsuarioController@listado')->name('usuarios.listado');
        Route::post('usuarios','UsuarioController@guardar')->name('usuarios.guardar');
        Route::get('usuarios/{user}/editar', 'UsuarioController@editar')->name('usuarios.editar');
        Route::put('usuarios/{user}', 'UsuarioController@actualizar')->name('usuarios.actualizar');

        //CLIENTES
        Route::get('clientes', 'ClienteController@listado')->name('clientes.listado');
        Route::post('clientes','ClienteController@guardar')->name('clientes.guardar');
        Route::get('clientes/{cliente}/editar', 'ClienteController@editar')->name('clientes.editar');
        Route::put('clientes/{cliente}', 'ClienteController@actualizar')->name('clientes.actualizar');
        Route::get('clientes/getClientesJson', 'ClienteController@getClientesJson')->name('clientes.getClientesJson');

        //PRODUCTOS
        Route::get('productos', 'ProductoController@listado')->name('productos.listado');
        Route::post('productos','ProductoController@guardar')->name('productos.guardar');
        Route::get('productos/{producto}/editar', 'ProductoController@editar')->name('productos.editar');
        Route::put('productos/{producto}', 'ProductoController@actualizar')->name('productos.actualizar');
        Route::get('productos/getProductosJson', 'ProductoController@getProductosJson')->name('productos.getClientesJson');

        //MARCAS
        Route::get('marcas/{nombre?}', 'MarcaController@guardar')->name('marcas.guardar');

        // PRESUPUESTOS
        Route::get('presupuestos', 'PresupuestoController@listado')->name('presupuestos.listado');
        Route::get('presupuestos/nuevo', 'PresupuestoController@crear')->name('presupuestos.crear');
        Route::post('presupuestos', 'PresupuestoController@guardar')->name('presupuestos.guardar');
        Route::get('presupuestos/{presupuesto}', 'PresupuestoController@ver')->name('presupuestos.ver');
        Route::get('presupuestos/{presupuesto}/editar', 'PresupuestoController@editar')->name('presupuestos.editar');
        Route::get('presupuestos/{presupuesto}/pdf', 'PresupuestoController@presupuesto')->name('presupuestos.verPdf');
        Route::post('presupuestos/{presupuesto}/enviar', 'PresupuestoController@enviarPdf')->name('presupuestos.enviarPdf');
    });


Auth::routes(['register' => false]);


