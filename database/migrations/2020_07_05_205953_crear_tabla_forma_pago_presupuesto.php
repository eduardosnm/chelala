<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaFormaPagoPresupuesto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forma_pago_presupuesto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('presupuesto_id');
            $table->unsignedBigInteger('forma_pago_id');
            $table->unsignedBigInteger('moneda_id')->nullable();
            $table->float('monto', 12,2)->nullable();
            $table->date('fecha')->nullable();
            $table->foreign('presupuesto_id')->references('id')->on('presupuestos')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('forma_pago_id')->references('id')->on('formas_de_pago')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('moneda_id')->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forma_pago_presupuesto');
    }
}
