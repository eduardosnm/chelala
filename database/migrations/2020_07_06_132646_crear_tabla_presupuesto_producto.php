<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPresupuestoProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuesto_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('producto_id');
            $table->unsignedBigInteger('presupuesto_id');
            $table->unsignedBigInteger('moneda_id');
            $table->unsignedBigInteger('cantidad');
            $table->float('precio_unitario', 12, 2);
            $table->float('subtotal', 12, 2);
            $table->foreign('producto_id')->references('id')->on('productos')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('presupuesto_id')->references('id')->on('presupuestos')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('moneda_id')->references('id')->on('monedas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuesto_producto');
    }
}
