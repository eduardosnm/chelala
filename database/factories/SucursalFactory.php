<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Sucursal::class, function (Faker $faker) {
    return [
        "nombre" => $faker->company,
        "direccion" => $faker->address,
        "telefono" => $faker->phoneNumber,
        "email" => $faker->email,
    ];
});
