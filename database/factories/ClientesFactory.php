<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Cliente::class, function (Faker $faker) {
    return [
        "razon_social" => $faker->company,
        "email" => $faker->email,
        "telefono" => $faker->phoneNumber,
        "cuit" => $faker->randomNumber(8),
        "user_id" => \App\User::inRandomOrder()->first()->id
    ];
});
