<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        "nombre" => $faker->word,
        "modelo" => $faker->word,
        "descripcion" => $faker->text,
        "marca_id" => \App\Marca::inRandomOrder()->first()->id,
    ];
});
