<?php

use Illuminate\Database\Seeder;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Sucursal::class)->create([
            "nombre" => "Casa Central",
            "direccion" => "Dorrego 528 - La Banda, Santiago del Estero",
            "telefono" => "385 427 1912 / 385 427 0063",
            "email" => "contacto@robertochelala.com.ar",
        ]);

        factory(\App\Sucursal::class)->create([
            "nombre" => "Fernandez",
            "direccion" => "Av. San Martin 628 - La Banda, Santiago del Estero",
            "telefono" => "385 491 1269",
        ]);

        factory(\App\Sucursal::class)->create([
            "nombre" => "Bandera",
            "direccion" => "Ruta Nacional 98 - Bandera, Santiago del Estero",
            "telefono" => "3857 421590 / 591",
        ]);

        factory(\App\Sucursal::class)->create([
            "nombre" => "Concepción",
            "direccion" => "Ruta 38 Km. 739, Tucumán",
            "telefono" => "3865492777",
        ]);
    }
}
