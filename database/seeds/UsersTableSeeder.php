<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(\App\User::class)->create([
            'nombre' => 'Eduardo',
            'apellido' => 'Bravo',
            'dni' => '12345678',
            'email' => 'eduardosnm@robertochelala.com',
            'password' =>bcrypt('123456'),
            'sucursal_id' => \App\Sucursal::inRandomOrder()->first()->id
        ]);

        $user->assign('admin');

        $user = factory(\App\User::class)->create([
            'nombre' => 'Gabriel',
            'apellido' => 'Anna',
            'dni' => '12345678',
            'email' => 'gabriel1@robertochelala.com.ar',
            'password' =>bcrypt('123456'),
            'sucursal_id' => 1
        ]);

        $user->assign('admin');

        $user = factory(\App\User::class)->create([
            'nombre' => 'Juan',
            'apellido' => 'Perez',
            'dni' => '87654321',
            'email' => 'juanperez@example.com',
            'password' =>bcrypt('123456'),
            'sucursal_id' => \App\Sucursal::inRandomOrder()->first()->id
        ]);

        $user->assign('operador');
    }
}
