<?php

use Illuminate\Database\Seeder;

class MonedasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Moneda::create([
            "nombre" => "Peso",
            "iso" => "ARS",
            "simbolo" => "$",
        ]);

        \App\Moneda::create([
            "nombre" => "Dólar",
            "iso" => "USD",
            "simbolo" => "US$",
        ]);

        \App\Moneda::create([
            "nombre" => "Euro",
            "iso" => "EUR",
            "simbolo" => "€",
        ]);
    }
}
