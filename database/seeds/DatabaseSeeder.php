<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BouncerSeeder::class);
        $this->call(SucursalesSeeder::class);
        $this->call(UsersTableSeeder::class);
//        $this->call(ClientesSeeder::class);
//        $this->call(MarcasSeeder::class);
        $this->call(MonedasSeeder::class);
        $this->call(EmpresaSeeder::class);
//        $this->call(ProductosSeeder::class);
        $this->call(FormasDePagoSeeder::class);
    }
}
