<?php

use Illuminate\Database\Seeder;

class FormasDePagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\FormaDePago::create([
            "nombre" => "A convenir"
        ]);

        \App\FormaDePago::create([
            "nombre" => "Efectivo"
        ]);

        \App\FormaDePago::create([
            "nombre" => "Cheque"
        ]);

        \App\FormaDePago::create([
            "nombre" => "Tarjeta"
        ]);

        \App\FormaDePago::create([
            "nombre" => "Pagaré"
        ]);

    }
}
