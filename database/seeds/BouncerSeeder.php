<?php

use App\Sucursal;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;
use Silber\Bouncer\Bouncer;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->crearRoles();

        $this->createAbilities();

        \Silber\Bouncer\BouncerFacade::allow('admin')->everything();
        \Silber\Bouncer\BouncerFacade::forbid('operador')->to('usuarios-listado');
        \Silber\Bouncer\BouncerFacade::forbid('operador')->to('usuarios-ver');
        \Silber\Bouncer\BouncerFacade::forbid('operador')->to('usuarios-nuevo');
        \Silber\Bouncer\BouncerFacade::forbid('operador')->to('usuarios-editar');
        \Silber\Bouncer\BouncerFacade::forbid('operador')->to('usuarios-eliminar');

    }

    protected function crearRoles(): void
    {
        \Silber\Bouncer\BouncerFacade::role()->create([
            'name' => 'admin',
            'title' => 'Administrador'
        ]);

        \Silber\Bouncer\BouncerFacade::role()->create([
            'name' => 'operador',
            'title' => 'Operador'
        ]);
    }

    protected function createAbilities()
    {
        \Silber\Bouncer\BouncerFacade::ability()->create([
            'name' => '*',
            'title' => 'Todas las habilidades',
            'entity_type' => '*',
        ]);

        \Silber\Bouncer\BouncerFacade::ability()->createForModel(\App\User::class, [
            'name' => 'listado',
            'title' => 'Listar usuarios',
        ]);

        \Silber\Bouncer\BouncerFacade::ability()->createForModel(\App\User::class, [
            'name' => 'crear',
            'title' => 'Crear usuario',
        ]);
    }
}
