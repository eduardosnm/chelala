<?php

namespace App;

use App\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRolesAndAbilities;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'sucursal_id');
    }
    /**
     * SCOPES
     */
    public function scopeBuscarUsuario($query, Request $request)
    {
        if ($request->has('buscar') && !is_null($request->get('buscar'))){
            $query->where('nombre', 'LIKE', "%{$request->get('buscar')}%")
                ->orWhere('apellido', 'LIKE', "%{$request->get('buscar')}%");
        }
        return $query;
    }

    /**
     * END SCOPES
     */

    /**
     * ACCESSORS AND MUTATORS
     */
    public function getFullNameAttribute()
    {
        return "{$this->nombre} {$this->apellido}";
    }


    /**
     * Send the password reset notification.
     *
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token, $this->email));
    }
}
