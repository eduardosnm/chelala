<?php

namespace App\Http\Controllers\Admin;

use App\Marca;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MarcaController extends Controller
{
    public function guardar(Request $request)
    {
        return Marca::create($request->all());
    }
}
