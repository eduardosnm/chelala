<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Sucursal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\BouncerFacade;

class UsuarioController extends Controller
{
    public function listado(Request $request)
    {
        BouncerFacade::authorize('usuarios-listado');
        $usuarios = User::buscarUsuario($request)
            ->paginate(20);

        $sucursales = Sucursal::all();

        return view('usuarios.listado', compact('usuarios','sucursales'));
    }

    public function guardar(UserRequest $request)
    {
        BouncerFacade::authorize('usuarios-nuevo');
        DB::transaction(function () use ($request) {
            $user = User::create([
                'nombre' => $request->get('nombre'),
                'apellido' => $request->get('apellido'),
                'email' => $request->get('email'),
                'dni' => $request->get('dni'),
                'sucursal_id' => $request->get('sucursal_id'),
                'password' => bcrypt($request->get('password')),
            ]);

            $user->assign($request->get('role'));
        });

        return redirect(route('usuarios.listado'))
            ->with('success', 'Operacion realizada correctamente');
    }

    public function editar(User $user)
    {
        BouncerFacade::authorize('usuarios-editar');
        return response()->json($user, 201);
    }

    public function actualizar(User $user, UserRequest $request)
    {
        BouncerFacade::authorize('usuarios-editar');
        if ($request->has('password')){
            $user->update(['password' => bcrypt($request->get('password'))]);
        }else{
            $user->update([
                'nombre' => $request->get('nombre'),
                'apellido' => $request->get('apellido'),
                'email' => $request->get('email'),
                'estado' => $request->has('estado') ? true : false,
            ]);
        }
        $user->assign($request->get('role'));

        return redirect(route('usuarios.listado'))->with("success", "El usuario se modificó correctamente");
    }

    public function eliminar(User $user)
    {
        BouncerFacade::authorize('usuarios-eliminar');
        $user->delete();

        return redirect(route('usuarios.listado'))
            ->with("success", "El usuario ha sido eliminado");
    }
}
