<?php

namespace App\Http\Controllers\Admin;

use App\Empresa;
use App\FormaDePago;
use App\FormaPagoPrespuesto;
use App\Http\Controllers\Controller;
use App\Marca;
use App\Moneda;
use App\Notifications\EnviarPresupuesto;
use App\Presupuesto;
use App\PresupuestoProducto;
use App\Sucursal;
use Barryvdh\DomPDF\Facade;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class PresupuestoController extends Controller
{
    public function listado(Request $request)
    {
        $presupuestos = Presupuesto::with("cliente", "productos")
            ->orderBy('created_at','DESC')
            ->paginate(20);

        return view('presupuestos.listado', compact('presupuestos'));
    }

    public function crear()
    {
        $monedas = Moneda::all();
        $formasPago = FormaDePago::all();
        $marcas = Marca::orderBy('nombre')->get();
        $sucursales = Empresa::all();

        return view('presupuestos.crear', compact('monedas', 'formasPago', 'marcas', 'sucursales'));
    }

    public function guardar(Request $request)
    {
        DB::beginTransaction();
        try {
            $pagos = collect($request->only('pagos')["pagos"]);

            $productos = $request->input('producto');

            $dataPresupuesto = $request->input('presupuesto');
            $dataPresupuesto["usuario_id"] = auth()->user()->id;
            $dataPresupuesto["monto"] = $productos["total"];
            unset($productos["total"]);
            $dataPresupuesto["sucursal_id"] = auth()->user()->sucursal->id;

            $presupuesto = Presupuesto::create($dataPresupuesto);

            $presupuesto->productos()->attach($productos);

            $presupuesto->formas_pago()->attach($pagos->toArray());

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
            return back()->withErrors($e->getMessage());
        }
        return redirect()->route("presupuestos.ver", $presupuesto)->with("success", "El presupuesto se creo con éxito");
    }

    public function ver(Presupuesto $presupuesto)
    {
        $presupuesto->load("cliente", "productos", "moneda");

        return view('presupuestos.ver', compact('presupuesto'));
    }

    public function editar(Presupuesto $presupuesto)
    {
        $monedas = Moneda::all();
        $formasPago = FormaDePago::all();
        $marcas = Marca::orderBy('nombre')->get();
        $presupuesto->load("cliente", "productos", "moneda");
        $otrasFormasPago = $presupuesto->formas_pago->where("id", "!=",3)->values();
        $cheques = $presupuesto->formas_pago->where("id", 3)->values();
        $sucursales = Empresa::all();

        return view('presupuestos.editar', compact('monedas', 'formasPago', 'marcas',
            'presupuesto','cheques', 'otrasFormasPago', 'sucursales'));
    }

    public function presupuesto(Presupuesto $presupuesto)
    {
        $pdf = $this->getPdf($presupuesto);

        return $pdf->stream('presupuesto.pdf');
    }

    public function enviarPdf(Presupuesto $presupuesto, Request $request)
    {
        $pdf = $this->getPdf($presupuesto);

        $data = $request->all();
        try {
            Mail::send([], [], function ($email) use ($pdf, $data) {
                $email->from(auth()->user()->email);
                $email->to($data["email"]);
                $email->subject("Presupuesto");
                $email->setBody('Adjuntamos prespuesto requerido');
                $email->setBody('Saludos!');
                $email->attachData($pdf->output(), "presupusto.pdf");
            });
        }catch (\Exception $e) {
            return back()->withErrors($e->getMessage());
        }

        return back()->with("success", "El correo fue enviado");
    }

    private function getPdf(Presupuesto $presupuesto)
    {
        $presupuesto->load("cliente", "productos", "moneda");

        $vista = view('presupuestos.template.presupuesto', compact('presupuesto'))->render();

        return Facade::loadHtml($vista);
    }
}
