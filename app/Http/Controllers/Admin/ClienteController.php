<?php

namespace App\Http\Controllers\Admin;

use App\Cliente;
use App\Http\Requests\ClienteRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    public function listado(Request $request)
    {
        $clientes = Cliente::buscar($request)
            ->orderBy('razon_social')
            ->paginate(20);

        return view('clientes.listado', compact('clientes'));
    }

    public function guardar(ClienteRequest $request)
    {
        $data = $request->all();
        $data["user_id"] = auth()->user()->id;

        DB::transaction(function () use ($data) {
            Cliente::create($data);
        });

        return back()->with("success", "Cliente creado con éxito");
    }

    public function editar(Cliente $cliente)
    {
        return view('clientes.editar', compact('cliente'));
    }

    public function actualizar(ClienteRequest $request, Cliente $cliente)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data["estado"] = isset($request->estado);
            $cliente->fill($data);

            if ($cliente->isClean()) {
                return redirect()->route('clientes.editar', $cliente)
                    ->withErrors("Al menos un valor debe cambiar");
            }

            $cliente->save();
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->route('clientes.editar', $cliente)
                ->withErrors($e->getMessage());
        }

        return redirect()->route('clientes.editar', $cliente)->with("success", "El cliente se modifico con éxito");
    }

    public function getClientesJson()
    {
        return Cliente::select('id', DB::raw("razon_social AS value"))
            ->where('estado',1)
            ->where("razon_social", "LIKE", "%{$_GET['term']}%")
            ->orWhere('cuit', "LIKE", "%{$_GET['term']}%")
            ->get()
            ->toJson();
    }
}
