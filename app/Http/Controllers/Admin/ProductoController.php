<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductoRequest;
use App\Imagen;
use App\Marca;
use App\Producto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductoController extends Controller
{
    public function listado(Request $request)
    {
        $productos = Producto::with('marca')
            ->buscar($request)
            ->orderBy("nombre")
            ->orderBy('marca_id')
            ->paginate(20);

        $marcas = Marca::all();

        return view('productos.listado', compact('productos', 'marcas'));
    }

    public function guardar(ProductoRequest $request)
    {
        DB::beginTransaction();
        try {
            $imagen = null;
            if ($request->hasFile('imagen')) {
                $imagen = $this->guardarImagen($request);
            }

            $producto = Producto::create([
                "nombre" => $request->nombre,
                "modelo" => $request->modelo,
                "descripcion" => $request->descripcion,
                "marca_id" => $request->marca_id,
                "imagen_id" => optional($imagen)->id
            ]);

            DB::commit();
        }catch (\Exception $ex) {
            DB::rollBack();
            if ($request->ajax()) {
                return response()->json(["data" => $ex->getMessage()], 500);
            }
            return back()->withErrors($ex->getMessage());
        }

        if ($request->ajax()) {
            return response()->json(["data" => $producto], 200);
        }
        return back()->with('success', 'El producto se creo con éxito');
    }

    private function guardarImagen(Request $request)
    {
        $image = $request->imagen;

        $extension = $image->getClientOriginalExtension();

        $filenametostore = time(). '.' . $extension;

        $image->move(public_path('images/productos/'),$filenametostore);

        return Imagen::create([
            'url' => 'public/images/productos/' . $filenametostore,
            'nombre' => $filenametostore,
            'mime' => $image->getClientMimeType(),
            'alt' => $request->nombre,
            'extension' => $extension,
            'descripcion' => strip_tags($request->descripcion)
        ]);
    }

    public function editar(Producto $producto)
    {
        $producto->load('marca', 'imagen');

        $marcas = Marca::all();

        return view('productos.editar', compact('producto', 'marcas'));
    }

    public function actualizar(ProductoRequest $request, Producto $producto)
    {
        DB::beginTransaction();
        try {
            $data = $request->except(['imagen']);

            $producto->fill($data);

            if ($request->hasFile('imagen')) {
                $producto->imagen()->dissociate();
                $producto->imagen()
                    ->associate($this->guardarImagen($request))
                    ->save();
            }
            $producto->save();
            DB::commit();
        }catch (\Exception $ex) {
            DB::rollBack();
            return redirect()->route('productos.editar', $producto)->withErrors($ex->getMessage());

        }

        return redirect()->route('productos.editar', $producto)->with('success', 'El producto se modificó con éxito');
    }

    public function getProductosJson()
    {
        return Producto::select('productos.id', DB::raw("CONCAT('Nombre: ',productos.nombre, ' - Modelo: ', modelo, ' - Marca: ', marcas.nombre) AS value"))
            ->join("marcas", "productos.marca_id", "=", "marcas.id")
            ->where("productos.nombre", "LIKE", "%{$_GET['term']}%")
            ->orWhere('modelo', "LIKE", "%{$_GET['term']}%")
            ->get()
            ->toJson();
    }
}
