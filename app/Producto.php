<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Producto extends Model
{
    protected $table = 'productos';
    protected $guarded = [];

    /**
     * RELACIONES
     */
    public function marca()
    {
        return $this->belongsTo(Marca::class, 'marca_id');
    }

    public function imagen()
    {
        return $this->belongsTo(Imagen::class, 'imagen_id', 'id');
    }

    public function presupuestos()
    {
        return $this->belongsToMany(Presupuesto::class, 'presupuesto_producto',
            'producto_id', 'presupuesto_id')
            ->using(PresupuestoProducto::class)
            ->withPivot('moneda_id', 'precio');
    }
    /**
     * FIN RELACIONES
     */

    /**
     * SCOPES
     */
    public function scopeBuscar($query, Request $request)
    {
        return $query->where("nombre", "LIKE", "%{$request->buscar}%")
            ->orWhere(function ($q) use ($request) {
                $q->where("modelo", "LIKE", "%{$request->buscar}%");
            })->orWhereHas('marca', function ($q) use ($request){
                $q->where("nombre", $request->buscar);
            });
    }
    /**
     * END SCOPES
     */

    /**
     * ACCESSORS Y MUTATOS
     */
    public function getDescExcerptAttribute()
    {
        return Str::words($this->descripcion,5);
    }

    public function getProductoCompletoAttribute()
    {
        return strtoupper("{$this->nombre} Modelo {$this->modelo} {$this->marca->nombre}");
    }
}
