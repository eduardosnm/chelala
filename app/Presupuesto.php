<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    protected $table = 'presupuestos';
    protected $guarded = [];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'presupuesto_producto',
            'presupuesto_id', 'producto_id')
            ->using(PresupuestoProducto::class)
            ->withPivot('presupuesto_id','producto_id','moneda_id', 'cantidad', 'precio_unitario', 'subtotal');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function moneda()
    {
        return $this->belongsTo(Moneda::class, 'moneda_id');
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'sucursal_id');
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }


    public function formas_pago()
    {
        return $this->belongsToMany(FormaDePago::class, 'forma_pago_presupuesto',
            'presupuesto_id', 'forma_pago_id')
            ->using(FormaPagoPrespuesto::class)
            ->withPivot('moneda_id', 'monto', 'fecha');
    }


    /**
     * ACCESSORS AND MUTATORS
     */
    public function getMontoTotalAttribute()
    {
        return $this->moneda->simbolo ." ". number_format($this->monto,2, ",", ".");
    }

    public function getNroCotizacionAttribute()
    {
        return str_pad($this->id, 10, "0", STR_PAD_LEFT);
    }

    public function getProductosAsociadosAttribute()
    {
        $nombres = [];
        foreach ($this->productos as $producto) {
            $nombres[] = $producto->producto_completo;
        }

        return implode(", ", $nombres);
    }
}
