<?php

namespace App;


use Illuminate\Database\Eloquent\Relations\Pivot;

class FormaPagoPrespuesto extends Pivot
{
    protected $table = 'forma_pago_presupuesto';
    protected $guarded = [];
    protected $dates = ['fecha'];

    public function moneda()
    {
        return $this->belongsTo(Moneda::class, 'moneda_id');
    }
}
