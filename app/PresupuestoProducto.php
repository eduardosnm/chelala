<?php

namespace App;


use Illuminate\Database\Eloquent\Relations\Pivot;

class PresupuestoProducto extends Pivot
{
    protected $table = 'presupuesto_producto';
    protected $fillable = ['presupuesto_id','producto_id','moneda_id', 'cantidad', 'precio_unitario', 'subtotal'];

    public function moneda()
    {
        return $this->belongsTo(Moneda::class, 'moneda_id');
    }
}
