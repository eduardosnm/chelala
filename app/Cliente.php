<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $guarded = [];

    /**
     * SCOPES
     */
    public function scopeBuscar($query, Request $request)
    {
        return $query->where("razon_social", "LIKE", "%{$request->buscar}%")
            ->orWhere(function ($q) use ($request) {
                $q->where("cuit", "LIKE", "%{$request->buscar}%");
            });
    }
    /**
     * END SCOPES
     */
}
