@extends('layouts.master')
@section('titulo', 'Nuevo presupuesto')
@section('contenido')
    <style>
        img {
            display: block;
            max-width:320px;
            max-height:180px;
            width: auto;
            height: auto;
        }
    </style>
    @include('partials.errors')
    @include('partials.success')

    <form id="crearPresupuestoForm" action="{{ route('presupuestos.guardar') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Seleccione un cliente</h6>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{ $presupuesto->cliente->razon_social }}" placeholder="Nombre o cuit del cliente" required id="cliente">
                            <input type="hidden" name="presupuesto[cliente_id]" value="{{ $presupuesto->cliente->id }}" id="cliente_id">
                            <div class="input-group-append">
                                <button id="nuevoUsuario" class="btn btn-success" title="Nuevo Cliente" data-toggle="modal" data-target="#crearUsuario">
                                    <i class="fas fa-user-plus"></i>
                                </button>
                            </div>
                            <div class="input-group-append">
                                <label class="input-group-text" for="inputGroupSelect02">
                                    <i class="fas fa-times" id="clienteMal" style="color:red"></i>
                                    <i class="fas fa-check" id="clienteBien" style="display:none; color:green"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Seleccione una razon social</h6>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <select class="custom-select" name="presupuesto[empresa_id]" id="empresa_id" required="">
                                <option selected="" disabled="">Seleccionar</option>
                                @foreach($sucursales as $sucursal)
                                    <option value="{{ $sucursal->id }}" @if($sucursal->id == $presupuesto->empresa_id) selected @endif>{{ $sucursal->nombre }} [{{ $sucursal->cuit }}]</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <label class="input-group-text" for="inputGroupSelect02">
                                    <i class="fas fa-cash-register"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Productos</h6>
                    </div>
                    <div class="card-body">
                        <div class="mb-2">
                            <button class="btn btn-primary btn-icon-split" id="nuevoProducto" data-toggle="modal" data-target="#crearProducto">
                                <span class="icon text-white-50">
                                    <i class="fas fa-tractor"></i>
                                </span>
                                <span class="text">Nuevo producto</span>
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <th>Producto</th>
                                <th>Moneda</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Subtotal</th>
                                <th></th>
                                </thead>
                                <tbody id="contenedorProductos">
                                @foreach($presupuesto->productos as $nro => $producto)
                                    <tr class="filaProducto">
                                        <td>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control producto" value="{{ $producto->nombre }}" placeholder="Nombre o modelo del producto" required>
                                                <input type="hidden" name="producto[{{ $nro }}][producto_id]" value="{{ $producto->id }}" class="producto_id" required>
                                                <div class="input-group-append">
                                                    <label class="input-group-text" for="inputGroupSelect02">
                                                        <i class="fas fa-times productoMal" id="productoMal" style="display:none;color:red"></i>
                                                        <i class="fas fa-check productoBien" id="productoBien" style="color:green"></i>
                                                    </label>
                                                </div>

                                            </div>
                                        </td>
                                        <td>
                                            <select class="form-control" name="producto[{{ $nro }}][moneda_id]" required>
                                                <option selected disabled>Seleccionar</option>
                                                @foreach($monedas as $moneda)
                                                    <option value="{{ $moneda->id }}" @if($moneda->id == $producto->pivot->moneda_id) selected @endif>{{ $moneda->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control cantidadProd" value="{{ $producto->pivot->cantidad }}" value="1" name="producto[{{ $nro }}][cantidad]" min="1" step="1" required>
                                        </td>
                                        <td class="contenedorPrecioUnitario">
                                            <input type="text" class="form-control precioUnitario" value="{{ number_format($producto->pivot->precio_unitario, 2, ".", ",") }}" required>
                                            <input type="hidden" class="form-control precio_unitario" value="{{ $producto->pivot->precio_unitario }}" name="producto[{{ $nro }}][precio_unitario]" required>
                                        </td>
                                        <td class="contenedorSubtotalProd">
                                            <input type="text" class="form-control subtotalProd" value="{{ number_format($producto->pivot->subtotal, 2, ".", ",") }}" required>
                                            <input type="hidden" class="form-control subtotal" value="{{ $producto->pivot->subtotal }}" name="producto[{{ $nro }}][subtotal]" required>
                                        </td>
                                        <td>
                                            <i class="fas fa-plus-circle fa-2x cursor agregarProducto" style="color:green" title="Agregar"></i>
                                            <i class="fas fa-minus-circle fa-2x cursor eliminarProducto" style="color:red;@if($loop->first) display: none @endif" title="Eliminar"></i>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr class="bg-primary text-white">
                                    <td colspan="3"></td>
                                    <td style="font-size: 2em"><b>Total</b></td>
                                    <td colspan="2" class="text-right" style="font-size: 2em" id="totalProductos">{{ number_format($presupuesto->monto, 2, ",", ".") }}</td>
                                    <input type="hidden" class="form-control" id="totalProd" value="{{ $presupuesto->monto }}" name="producto[total]" required>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Condiciones comerciales</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                <label for="">Validez de la oferta</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" name="presupuesto[validez]" placeholder="Validez de la oferta en dias"
                                           required id="cliente" min="1" step="1" value="{{ $presupuesto->validez }}">
                                    <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon3">
                                    <i class="fas fa-calendar-alt"></i>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                <label for="">IVA</label>
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="presupuesto[iva]" id="iva" required>
                                        <option selected disabled>Seleccionar</option>
                                        <option value="con_iva" @if($presupuesto->iva == "con_iva") selected @endif>Con IVA</option>
                                        <option value="sin_iva" @if($presupuesto->iva == "sin_iva") selected @endif>Sin IVA</option>
                                    </select>
                                    <div class="input-group-append">
                                        <label class="input-group-text" for="inputGroupSelect02">
                                            <i class="fas fa-cash-register"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                <label for="">Plazo de entrega</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" placeholder="Plazo de entrega en dias"
                                           required min="0" name="presupuesto[plazo_entrega]" value="{{ $presupuesto->plazo_entrega }}">
                                    <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon3">
                                    <i class="fas fa-clock"></i>
                                </span>
                                    </div>
                                </div>
                                <span class="text-info">* Para entrega inmediata ingresar 0 (cero)</span>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                <label for="">Flete</label>
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="presupuesto[flete]" id="flete" required>
                                        <option selected disabled>Seleccionar</option>
                                        <option value="bonificado" @if($presupuesto->flete == "bonificado") selected @endif>Bonificado</option>
                                        <option value="cliente" @if($presupuesto->flete == "cliente") selected @endif>A cargo del cliente</option>
                                    </select>
                                    <div class="input-group-append">
                                        <label class="input-group-text" for="inputGroupSelect02">
                                            <i class="fas fa-shipping-fast"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4" style="min-height: 173px">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Pagos</h6>
                    </div>
                    <div class="card-body">
                        @foreach($otrasFormasPago as $nro => $forma_pago)
                            <div class="contenedorPago mt-3" style="border-bottom: 1px solid #b72d2b; padding-bottom: 2em">
                                <div class="row">
                                    <div class="col-sm">
                                        <label for="moneda_id">Forma de pago</label>
                                        <div class="input-group mb-3">
                                            <select class="custom-select formaPago limpiar" name="pagos[{{ intval($nro) }}][forma_pago_id]" id="formaPago">
                                                <option selected disabled>Seleccionar</option>
                                                @foreach($formasPago as $formaPago)
                                                    <option value="{{ $formaPago->id }}" @if($formaPago->id == $forma_pago->id) selected @endif>{{ $formaPago->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="inputGroupSelect02">
                                                    <i class="fas fa-cash-register"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm contenedorMoneda">
                                        <label for="moneda_id">Moneda</label>
                                        <div class="input-group mb-3">
                                            <select class="custom-select moneda limpiar" name="pagos[{{ intval($nro) }}][moneda_id]" id="moneda_id">
                                                <option selected disabled>Seleccionar moneda</option>
                                                @foreach($monedas as $moneda)
                                                    <option value="{{ $moneda->id }}" @if($moneda->id == $forma_pago->pivot->moneda_id) selected @endif>{{ $moneda->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="inputGroupSelect02">
                                                    <i class="fas fa-comment-dollar"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm contenedorImporte">
                                        <label for="moneda_id">Importe</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control importe limpiar" value="{{ number_format($forma_pago->pivot->monto, 2, ",", ".") }}">
                                            <input type="hidden" class="monto limpiar" name="pagos[{{ intval($nro) }}][monto]" value="{{ $forma_pago->pivot->monto }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon3">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-1 col-12 m-auto">
                                        <i class="fas fa-plus-circle fa-2x cursor agregarPago" style="color:green" title="Agregar"></i>
                                        <i class="fas fa-minus-circle fa-2x cursor eliminarPago" style="color:red; @if($loop->first) display:none @endif" title="Eliminar"></i>
                                    </div>
                                </div>

                                <div class="row chequesContainer" style="display: none">

                                    <div class="offset-sm-5 col-1 text-center">
                                        <label>Cheque Nº</label>
                                        <p class="nroCheque" style="font-weight: bold; font-size: 1.8em">1</p>
                                        <input type="hidden" class="chequeforma_pago_id" name="pagos[0][forma_pago_id]" value="3" disabled/>
                                        <input type="hidden" class="chequemoneda_id" name="pagos[0][moneda_id]" value="1" disabled/>
                                    </div>

                                    <div class="col-sm">
                                        <label for="moneda_id">Importe</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control importe limpiar" required>
                                            <input type="hidden" class="form-control monto limpiar" name="pagos[0][monto]" required disabled>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon3">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm">
                                        <label for="moneda_id">Fecha</label>
                                        <div class="input-group mb-3">
                                            <input type="date" class="form-control fecha limpiar" disabled name="pagos[0][fecha]" required>
                                            <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon3">
                                            <i class="fas fa-calendar-alt"></i>
                                        </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-1" style="padding-top: 2.5em;">
                                        <i class="fas fa-plus-square fa-2x cursor agregarCheque" style="color:blue" title="Agregar cheque"></i>
                                        <i class="fas fa-minus-square fa-2x cursor eliminarCheque" style="color:red; display: none" title="Eliminar cheque"></i>
                                    </div>
                                </div>

                            </div>
                        @endforeach

                        @if($cheques->count() != 0)
                            <div class="contenedorPago mt-3" style="border-bottom: 1px solid #b72d2b; padding-bottom: 2em">
                                <div class="row">
                                    <div class="col-sm">
                                        <label for="moneda_id">Forma de pago</label>
                                        <div class="input-group mb-3">
                                            <select class="custom-select formaPago limpiar" name="pagos[{{ intval($otrasFormasPago->count()) }}][forma_pago_id]" id="formaPago">
                                                <option selected disabled>Seleccionar</option>
                                                @foreach($formasPago as $formaPago)
                                                    <option value="{{ $formaPago->id }}" @if($formaPago->id == 3) selected @endif>{{ $formaPago->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="inputGroupSelect02">
                                                    <i class="fas fa-cash-register"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm contenedorMoneda" @if($cheques->count() != 0) style="display:none" @endif>
                                        <label for="moneda_id">Moneda</label>
                                        <div class="input-group mb-3">
                                            <select class="custom-select moneda limpiar" name="pagos[{{ intval($otrasFormasPago->count() ) }}][moneda_id]" id="moneda_id"
                                                    @if($cheques->count() != 0) disabled @endif>
                                                <option selected disabled>Seleccionar moneda</option>
                                                @foreach($monedas as $moneda)
                                                    <option value="{{ $moneda->id }}" @if($moneda->id == 1) selected @endif>{{ $moneda->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append">
                                                <label class="input-group-text" for="inputGroupSelect02">
                                                    <i class="fas fa-comment-dollar"></i>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm contenedorImporte" @if($cheques->count() != 0) style="display:none" @endif>
                                        <label for="moneda_id">Importe</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control importe limpiar" @if($cheques->count() != 0) disabled @endif>
                                            <input type="hidden" class="form-control monto limpiar" name="pagos[{{ intval($otrasFormasPago->count() ) }}][monto]" @if($cheques->count() != 0) disabled @endif>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon3">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-1 col-12 m-auto">
                                        <i class="fas fa-plus-circle fa-2x cursor agregarPago" style="color:green" title="Agregar"></i>
                                        <i class="fas fa-minus-circle fa-2x cursor eliminarPago" style="color:red;" title="Eliminar"></i>
                                    </div>
                                </div>
                                @foreach($cheques as $nro => $forma_pago)
                                    <div class="row chequesContainer" @if($cheques->count() == 0) style="display: none" @endif>

                                        <div class="offset-sm-5 col-1 text-center">
                                            <label>Cheque Nº</label>
                                            <p class="nroCheque" style="font-weight: bold; font-size: 1.8em">{{ intval($nro + 1) }}</p>
                                            <input type="hidden" class="chequeforma_pago_id" name="pagos[{{intval($otrasFormasPago->count() + $nro)}}][forma_pago_id]" value="3"/>
                                            <input type="hidden" class="chequemoneda_id" name="pagos[{{intval($otrasFormasPago->count() + $nro)}}][moneda_id]" value="1"/>
                                        </div>

                                        <div class="col-sm">
                                            <label for="moneda_id">Importe</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control importe limpiar" required value="{{ number_format($forma_pago->pivot->monto, 2, ",", ".") }}">
                                                <input type="hidden" class="form-control monto limpiar" name="pagos[{{ intval($otrasFormasPago->count() + $nro) }}][monto]" required
                                                       value="{{ $forma_pago->pivot->monto }}">
                                                <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon3">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm">
                                            <label for="moneda_id">Fecha</label>
                                            <div class="input-group mb-3">
                                                <input type="date" class="form-control fecha limpiar" name="pagos[{{ intval($otrasFormasPago->count() + $nro) }}][fecha]"
                                                       required value="{{ optional($forma_pago->pivot->fecha)->format("Y-m-d") }}">
                                                <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon3">
                                                    <i class="fas fa-calendar-alt"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-1" style="padding-top: 2.5em;">
                                            <i class="fas fa-plus-square fa-2x cursor agregarCheque" style="color:blue" title="Agregar cheque"></i>
                                            <i class="fas fa-minus-square fa-2x cursor eliminarCheque" style="color:red; @if($loop->first) display: none @endif" title="Eliminar cheque"></i>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <button id="crearPresupuestoBtn" type="submit" class="btn btn-primary btn-block">Guardar</button>
            </div>
        </div>
    </form>

    <div class="modal fade bd-example-modal-lg" id="crearUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crear cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="crearUsuarioForm" action="{{ route('clientes.guardar') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="container-fluid">
                            @include('clientes.partials.form-crear')
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="crearProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crear producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="crearProductoForm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="container-fluid">
                            @include('productos.partials.form-crear',  ["marcas" => $marcas])
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="btn_submit" class="btn btn-primary">Guardar <span id="crearProdSpinner" style="display: none"><i class="fas fa-spinner fa-spin"></i></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="crearMarcaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Crear marca</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" name="nombre" id="nombreMarca">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="guardarMarca" disabled class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('tinymce/tinymce.js') }}"></script>
    <script>
        $(function () {
            $("#cuit").mask("00-00000000-0");

            $('.subtotalProd, .precioUnitario, .importe').mask("#.##0,00", {reverse: true});

            let totalProd = 0;

            let totalProductos = function () {
                let subtotal = 0;
                $(".subtotalProd").each( function (index, input) {
                    let monto = $(input).val().replace(".","").replace(",",".");
                    $(this).closest(".filaProducto").find(".subtotal").val(monto);
                    subtotal += parseFloat(monto);
                });
                totalProd = subtotal;
                $("#totalProd").val(totalProd);
                $("#totalProductos").html(totalProd.toFixed(2).toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    .replace(".",",").replace(",",".")
                );
            }


            $(document).on("change",".precioUnitario, .cantidadProd", function (e) {
                e.preventDefault();
                let $fila = $(this).closest(".filaProducto");
                let cantidad = parseInt($fila.find(".cantidadProd").val());
                let precio = parseFloat($fila.find(".precioUnitario").val().replace(".","").replace(",","."));
                let subtotal = cantidad*precio;
                if (isNaN(subtotal)) {
                    return;
                }
                console.log(subtotal)
                $fila.find(".precio_unitario").val(precio);
                $fila.find(".subtotalProd").val(subtotal.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    .replace(".",",").replace(",","."));
                $fila.find(".subtotalProd").change();
            });

            let totalPagos = parseFloat("{{ $presupuesto->monto }}");
            let sumarPagos = function () {
                let subtotal = 0;
                $(".importe").each( function (index, input) {
                    let monto = parseFloat($(input).val().replace(".","").replace(",","."));
                    if(isNaN(monto)){
                        return true;
                    }
                    $(this).closest("div").find(".monto").val(monto);
                    subtotal += monto;
                });
                totalPagos = subtotal;

                $("#crearPresupuestoBtn").prop("disabled",!(totalPagos === totalProd));
                console.log(totalPagos, totalProd);
            }

            $(document).on("change",".subtotalProd", function (e) {
                e.preventDefault();
                totalProductos();
                sumarPagos();
            });

            $(document).on("change",".importe", function (e) {
                e.preventDefault();
                sumarPagos();
            });
            totalProductos();
            sumarPagos();

            $("#crearPresupuestoBtn").click(function (e) {
                e.preventDefault();
                $("#crearPresupuestoForm").submit();
            })

            $(".formaPago").on("change", function () {
                let formaPagoId = parseInt($(this).val());
                let $contenedorPago = $(this).closest(".contenedorPago");
                sumarPagos();
                if(formaPagoId == 3){
                    $contenedorPago.find(".contenedorImporte").hide();
                    $contenedorPago.find(".contenedorMoneda").hide();
                    $contenedorPago.find(".contenedorImporte :input").val(null).prop("disabled",true);
                    $contenedorPago.find(".contenedorMoneda .moneda").prop("disabled",true);
                    $contenedorPago.find(".chequesContainer").show();
                    $contenedorPago.find(".chequesContainer :input").prop("disabled",false);
                    $contenedorPago.find(".cantidadCheques").focus();
                    return;
                }else{
                    $contenedorPago.find(".contenedorImporte").show();
                    $contenedorPago.find(".contenedorMoneda").show();
                    $contenedorPago.find(".contenedorMoneda .moneda").prop("disabled",false)
                    $contenedorPago.find(".chequesContainer").not(".chequesContainer:first").remove();
                    $contenedorPago.find(".chequesContainer").hide();
                    $contenedorPago.find(".chequesContainer :input").val(null);
                    $contenedorPago.find(".chequesContainer :input").prop("disabled",true);
                    $contenedorPago.find(".contenedorImporte :input").prop("disabled",false);
                    $contenedorPago.find(".contenedorImporte :input").prop('required', true);
                    $contenedorPago.find(".moneda").prop("required",true);

                }

                if(formaPagoId == 1) {
                    $contenedorPago.find(".contenedorImporte :input").prop("required",false);
                    $contenedorPago.find(".moneda").prop("required",false);
                    return;
                }
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagenProducto').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.custom-file-input').on('change',function(e){
                let fileName = $("#imagen").get(0).files[0].name;
                let nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
                if(this.files[0].size > 2097152){
                    alert("¡La imagen es demasiado pesada! Seleccione una imagen menor a 1MB.");
                    this.value = "";

                };
                readURL(this);
            });

            let cheques = function ($contenedorPago) {
                $contenedorPago.find(".chequesContainer").each(function (i, value) {
                    $(value).find(".nroCheque").html(++i);
                })
            }

            var cantidadPagos = "{{ ($otrasFormasPago->count() + $cheques->count()) }}";

            $(".agregarCheque").on("click", function (e) {
                e.preventDefault();
                let $contenedorPago = $(this).closest(".contenedorPago");
                let $chequesContainer = $(this).closest(".chequesContainer").clone(true,true);
                $chequesContainer.find(".eliminarCheque").show();
                $chequesContainer.find(".limpiar").val(null);
                $chequesContainer.find("chequeforma_pago_id").val(3);
                $chequesContainer.find("chequemoneda_id").val(1);
                $chequesContainer.find('input.importe').remove();
                var $inputs = $chequesContainer.find(':input[name*="pago"]');
                var matches = 0;
                $inputs.each(function (index, element) {
                    matches = $(element).attr("name").match(/\[(.*?)\]/);
                    var new_name = "pagos["+(cantidadPagos)+"]";
                    var current_name = $(element).attr("name");
                    new_name = current_name.replace("pagos["+matches[1]+"]", new_name);
                    $(element).attr("name", new_name);
                });
                $('<input type="text" class="form-control importe" required />')
                    .mask("#.##0,00", {reverse: true})
                    .insertBefore($chequesContainer.find(".monto"));
                $contenedorPago.append($chequesContainer);
                cheques($contenedorPago);
                cantidadPagos++;
            });

            $(".eliminarCheque").on('click', function (e) {
                e.preventDefault();
                let $contenedorPago = $(this).closest(".contenedorPago");
                if(confirm("¿Esta seguro que desea eliminar este cheque?")) {
                    $(this).closest(".chequesContainer").remove();
                    sumarPagos();
                }
                cheques($contenedorPago)
            });


            $(".agregarPago").on("click", function (e) {
                e.preventDefault();
                cantidadPagos++;
                let $contenedorPago = $(this).closest(".contenedorPago").clone(true,true);
                $contenedorPago.find(".chequesContainer").not(".chequesContainer:first").remove();
                $contenedorPago.find(".chequesContainer").hide();
                $contenedorPago.find('input.importe').remove();
                $contenedorPago.find(':input').val(null);
                $contenedorPago.find(".eliminarPago").show();
                var $inputs = $contenedorPago.find(':input[name*="pago"]');
                var matches = 0;
                $inputs.each(function (index, element) {
                    matches = $(element).attr("name").match(/\[(.*?)\]/);
                    var new_name = "pagos["+(cantidadPagos)+"]";
                    var current_name = $(element).attr("name");
                    new_name = current_name.replace("pagos["+matches[1]+"]", new_name);
                    $(element).attr("name", new_name);
                });
                $('<input type="text" class="form-control importe" required />')
                    .mask("#.##0,00", {reverse: true})
                    .insertBefore($contenedorPago.find(".contenedorImporte").find(".monto"));
                $('<input type="text" class="form-control importe" required />')
                    .mask("#.##0,00", {reverse: true})
                    .insertBefore($contenedorPago.find(".chequesContainer").find(".monto"));
                $(this).closest('.card-body').append($contenedorPago);
            });

            $(".eliminarPago").on('click', function (e) {
                e.preventDefault();
                if(confirm("¿Esta seguro que desea eliminar este pago?")) {
                    $(this).closest(".contenedorPago").remove();
                    sumarPagos();
                }
            });

            $("#cliente").autocomplete({
                source: "{{route('clientes.getClientesJson')}}",
                minLength: 3,
                select: function (event, ui) {
                    $('#cliente_id').val(ui.item.id);
                    $('#clienteMal').hide();
                    $('#clienteBien').show();
                }
            });

            $("#cliente").keyup(function(e) {
                e.preventDefault();
                if ($(this).val() == '' || $('#cliente_id').val() == '') {
                    $('#clienteMal').show();
                    $('#clienteBien').hide();
                }
            });

            $(".producto").autocomplete({
                source: "{{route('productos.getClientesJson')}}",
                minLength: 3,
                select: function (event, ui) {
                    $(this).next('.producto_id').val(ui.item.id);
                    $(this).closest(".input-group").find('.productoMal').hide();
                    $(this).closest(".input-group").find('.productoBien').show();
                }
            });

            $(".input-group").on("keyup",".producto",function(e) {
                e.preventDefault();
                console.log("ntra")
                if ($(this).val() == '' || $('#producto_id').val() == '') {
                    $(this).closest(".input-group").find('.productoMal').show();
                    $(this).closest(".input-group").find('.productoBien').hide();
                }
            });

            let cantidadProductos = $(".filaProducto").length;
            $(".agregarProducto").on("click", function (e) {
                e.preventDefault();
                cantidadProductos++;
                let $contenedorProducto = $(this).closest(".filaProducto").clone(true,true);
                $contenedorProducto.find(":input").val(null);
                $contenedorProducto.find(".eliminarProducto").show();
                $contenedorProducto.find(".eliminarProducto");
                $contenedorProducto.find('input.producto').remove();
                $contenedorProducto.find('input.precioUnitario').remove();
                $contenedorProducto.find('input.subtotalProd').remove();
                $contenedorProducto.find('.productoMal').show();
                $contenedorProducto.find('.productoBien').hide();
                var $inputs = $contenedorProducto.find(':input[name*="producto"]');
                var matches = 0;
                $inputs.each(function (index, element) {
                    matches = $(element).attr("name").match(/\[(.*?)\]/);
                    var new_name = "producto["+(cantidadProductos)+"]";
                    var current_name = $(element).attr("name");
                    new_name = current_name.replace("producto["+matches[1]+"]", new_name);
                    $(element).attr("name", new_name);
                });
                $contenedorProducto.find(".contenedorPrecioUnitario").append(
                    $('<input type="text" class="form-control precioUnitario" required />')
                        .mask("#.##0,00", {reverse: true})
                );
                $contenedorProducto.find(".contenedorSubtotalProd").append(
                    $('<input type="text" class="form-control subtotalProd" required>')
                        .mask("#.##0,00", {reverse: true})
                );
                $('<input type="text" class="form-control producto" id="producto'+cantidadProductos+'" name="producto['+cantidadProductos+'][producto]" placeholder="Nombre o modelo del producto">')
                    .autocomplete({
                        source: "{{route('productos.getClientesJson')}}",
                        minLength: 3,
                        select: function (event, ui) {
                            $(this).next('.producto_id').val(ui.item.id);
                            $(this).closest(".input-group").find('.productoMal').hide();
                            $(this).closest(".input-group").find('.productoBien').show();
                        }
                    }).insertBefore($contenedorProducto.find(".producto_id"));
                $("#contenedorProductos").append($contenedorProducto);
            });

            $(".eliminarProducto").on('click', function (e) {
                e.preventDefault();
                if(confirm("¿Esta seguro que desea eliminar este producto?")) {
                    $(this).closest(".filaProducto").remove();
                    totalProductos();
                    sumarPagos();
                }
            });

            tinymce.init({
                selector: '#descripcion',
                language: 'es',
                content_style: "body { font-family: Helvetica Neue Light; font-size: 14px}"
            });

            $("#crearProducto").on("hide.bs.modal", function (e) {
                $("#btn_submit").prop("disabled", false);
            })

            $('#crearMarca').on("click", function(e){
                e.preventDefault();

                $('#crearProducto').modal('hide');
                $('#crearMarcaModal').modal('show');
            });

            $("#nombreMarca").on("keyup", function (e) {
                e.preventDefault();
                if ($(this).val() != '') {
                    $("#guardarMarca").prop("disabled", false);
                }else {
                    $("#guardarMarca").prop("disabled", true);
                }
            });

            $("#guardarMarca").on("click", function (e) {
                e.preventDefault();
                $("#crearMarca").prop("disabled", true);
                let nombre = $("#nombreMarca").val();
                let url = "{{ route('marcas.guardar') }}";

                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: "json",
                    data: {
                        nombre: nombre
                    }
                }).done(function (result) {
                    $("#marca_id").append('<option value="'+result.id+'" selected>'+result.nombre+'</option>');
                    $('#crearMarcaModal').modal('hide');
                    $('#crearProducto').modal('show');
                    $("#crearMarca").prop("disabled", false);
                }).fail(function (result) {
                    console.log(result);
                })
            })

            $("#nuevoUsuario, #nuevoProducto").click(function(e) {
                e.preventDefault();
            });

            $("#btn_submit").on("click", function(e){
                e.preventDefault();
                var myForm = new FormData($("#crearProductoForm")[0]);
                var myContent = tinymce.activeEditor.getContent();
                $(this).prop("disabled", true);
                $("#crearProdSpinner").show();
                myForm.append("descripcion", myContent);
                $.ajax({
                    url: "{{ route('productos.guardar') }}",
                    method: "POST",
                    data: myForm,
                    dataType: "JSON",
                    contentType:false,
                    cache: false,
                    processData: false
                }).done(function(result){
                    $("#crearProducto").modal("hide");
                    $(this).prop("disabled", false);
                    $("#crearProdSpinner").hide();
                    $("#successAlert").show();
                    setTimeout(function (){
                        $("#successAlert").hide();
                    }, 5000) ;
                }).fail(function(result){
                    $("#crearProducto").modal("hide");
                    $(this).prop("disabled", false);
                    $("#crearProdSpinner").hide();
                    $("#errores").empty();
                    let errores = result.responseJSON.errors;
                    let msj = "";
                    for (error in errores){
                        msj += "<li>"+errores[error][0]+"</li>";
                    }
                    $("#errores").append(msj);
                    $("#errorAlert").show();
                    setTimeout(function (){
                        $("#errorAlert").hide();
                    }, 5000) ;
                });
            });

        });
    </script>
@endsection
