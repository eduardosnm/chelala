@extends('layouts.master')
@section('titulo', "Cotizacion Nº ".$presupuesto->id)
@section('contenido')
    @include('partials.errors')
    @include('partials.success')
    <style>
        img {
            display: block;
            max-width:320px;
            max-height:180px;
            width: auto;
            height: auto;
        }
    </style>

    <div class="row mb-4">
        <div class="col col-12">
            <a href="{{ route('presupuestos.verPdf', $presupuesto) }}" target="_blank" class="btn btn-success btn-icon-split" >
                    <span class="icon text-white-50">
                        <i class="fas fa-print"></i>
                    </span>
                <span class="text">Imprimir</span>
            </a>

            <button class="btn btn-info btn-icon-split" data-toggle="modal" data-target="#enviarPresupuesto">
                    <span class="icon text-white-50">
                        <i class="fas fa-envelope"></i>
                    </span>
                <span class="text">Enviar por correo</span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Cliente</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Razon social: </b>{{ $presupuesto->cliente->razon_social }}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Correo: </b>{{ $presupuesto->cliente->email }}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Teléfono: </b>{{ $presupuesto->cliente->telefono }}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Cuit: </b>{{ $presupuesto->cliente->cuit }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Productos</h6>
                </div>
                <div class="card-body">
                    @foreach($presupuesto->productos as $producto)
                        <div class="row mb-4 pb-3" @if(!$loop->last) style="border-bottom: 1px solid #ebecf0" @endif>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                <p class="text-uppercase"><b>{{ $producto->producto_completo }}</b></p>
                                <p class="text-uppercase">
                                    <b>
                                        Precio: {{ $producto->pivot->cantidad }} X {{ $producto->pivot->moneda->simbolo }} {{ $producto->pivot->subtotal }}
                                    </b>
                                </p>
                                <p class="text-uppercase"><b>Descripción</b></p>
                                {!! $producto->descripcion !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-12 text-center">
                                @if($producto->imagen()->exists())
                                    <img src="{{ asset('images/productos') }}/{{$producto->imagen->nombre}}" alt=""  >
                                @else
                                    <img src="{{ asset('images/Imagen-no-disponible.png') }}" alt="" class="img-fluid img-thumbnail">
                                @endif

                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Condiciones comerciales</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Validez de la oferta: </b>{{ $presupuesto->validez }} dias</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>IVA: </b>{{ $presupuesto->iva == 'con_iva' ? 'Incluye IVA' : 'No incluye IVA' }}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Plazo de entrega: </b>{{ $presupuesto->plazo_entrega == 0 ? 'Inmediata' : $presupuesto->plazo_entrega .' dias' }}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                            <p class="text-uppercase"><b>Flete: </b>{{ $presupuesto->flete }}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4" style="min-height: 173px">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Forma de pago</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <th>Forma de pago</th>
                            <th>Monto</th>
                            <th>Fecha</th>
                            </thead>
                            <tbody>
                            @foreach($presupuesto->formas_pago as $formaPago)
                                <tr>
                                    <td>{{ $formaPago->nombre }}</td>
                                    <td>{{ optional($formaPago->pivot->moneda)->simbolo }} {{ number_format($formaPago->pivot->monto, 2, ",",".") }}</td>
                                    <td>{{ optional($formaPago->pivot->fecha)->format("d/m/Y") }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr class="bg-danger text-white font-weight-bold" style="font-size: 1.6em">
                                <td>Total</td>
                                <td colspan="2">{{ number_format($presupuesto->monto, 2, ",",".") }}</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="enviarPresupuesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enviar presupuesto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('presupuestos.enviarPdf', $presupuesto) }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Correo electrónico</label>
                            <input type="email" class="form-control" name="email" required value="{{ $presupuesto->cliente->email }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
