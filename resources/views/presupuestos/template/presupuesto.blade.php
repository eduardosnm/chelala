<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presupuesto</title>

    <style type="text/css">
        body {
            font-size: 12px;
            font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial, sans-serif;
        }
        p{
            margin:0;
            padding:0;
        }
        .imgProducto {
            display: block;
            max-width:250px;
            max-height:110px;
            width: auto;
            height: auto;
            padding-top: 100px;
        }

        #productos {
            margin-top: 50px;
        }

        tr.border_bottom td {
            border-bottom:1pt solid black;
            margin-top: 100px;
        }
    </style>
</head>
<body>
    <table width="100%">
        <tr>
            <td valign="left" width="5%">
                <img src="{{public_path('images/logo-horizontal.png')}}" alt="" width="150" style="margin: 0"/>
            </td>


            <td width="100%" align="right">
                <h3 style="margin-top: 0px;">{{ $presupuesto->empresa->nombre }}</h3>

                    <strong>CUIT: {{ $presupuesto->empresa->cuit }}</strong>
                    <p>{{ $presupuesto->empresa->direccion }}</p>
                    <p>Tel {{ $presupuesto->sucursal->telefono }}</p>
                    <p>{{ $presupuesto->sucursal->email }}</p>

            </td>
        </tr>
    </table>

    <table width="100%" style="border-top: 2px solid black;">
        <tr>
            <td align="left">
                <strong>Sres. {{ $presupuesto->cliente->razon_social }}</strong>
            </td>
            <td align="right">
                <strong>Santiago del estero, {{ \Carbon\Carbon::now()->day }} de {{ \Carbon\Carbon::now()->monthName }} de {{ \Carbon\Carbon::now()->year }}</strong>
            </td>
        </tr>
        <tr>
            <td align="left">
                <a href="mailto:{{ $presupuesto->cliente->email }}">{{ $presupuesto->cliente->email }}</a>
                <div><strong>{{ $presupuesto->cliente->telefono }}</strong></div>
                <div>Atte.</div>

            </td>
            <td align="right">
                Cotizacion Nº {{ $presupuesto->nro_cotizacion }}
            </td>
        </tr>
    </table>

    <br>


        @foreach($presupuesto->productos as $producto)
            <div style="clear: left">

                <div style="float:left; width: 450px">
                        <div><strong>{{ $producto->producto_completo }}</strong></div>
                        <div>
                            <strong>
                                PRECIO: {{ $producto->pivot->cantidad }} X {{ $producto->pivot->moneda->simbolo }} {{ number_format($producto->pivot->subtotal, 2, ",", ".") }} ({{ $presupuesto->iva =='con_iva' ? 'IVA Incluido' : 'Sin IVA' }})
                            </strong>
                        </div>
                        <div>
                            {!! $producto->descripcion !!}
                        </div>
                    </div>

                @if($producto->imagen()->exists())
                    <div style="float:left; height: 120px; width: 200px">
                        <img style="margin-top: -100px" src="{{ public_path('images/productos') }}/{{$producto->imagen->nombre}}" class="imgProducto" alt="">
                    </div>
                @endif
            </div>
            <hr style="clear: left; border: 0.5px solid black;">
        @endforeach


{{--    <table width="100%">--}}
{{--        @foreach($presupuesto->productos as $producto)--}}
{{--            <tr class="border_bottom">--}}
{{--                <td style="padding-bottom: 20px">--}}
{{--                    <div><strong>{{ $producto->producto_completo }}</strong></div>--}}
{{--                    <div>--}}
{{--                        <strong>--}}
{{--                            PRECIO: {{ $producto->pivot->moneda->simbolo }} {{ $producto->pivot->precio }} ({{ $presupuesto->iva =='con_iva' ? 'IVA Incluido' : 'Sin IVA' }})--}}
{{--                        </strong>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        {!! $producto->descripcion !!}--}}
{{--                    </div>--}}
{{--                </td>--}}

{{--                @if($producto->imagen()->exists())--}}
{{--                    <td align="center" >--}}
{{--                        <img style="margin-top: -100px" src="{{ public_path('images/productos') }}/{{$producto->imagen->nombre}}" class="imgProducto" alt="">--}}
{{--                    </td>--}}
{{--                @endif--}}
{{--            </tr>--}}
{{--        @endforeach--}}
{{--    </table>--}}
    <br>
    <h4 align="center">CONDICIONES GENERALES</h4>
    <br>
    <div><strong>VALIDEZ DE LA OFERTA: </strong>{{ $presupuesto->validez }} días corridos.</div>
    <div><strong>PLAZO DE ENTREGA: </strong>{{ $presupuesto->plazo_entrega == 0 ? 'Inmediata' : $presupuesto->plazo_entrega . " días" }}.</div>
    <div><strong>FORMA DE PAGO</strong></div>
    <table width="100%" border="1">
        <thead style="background-color: lightgray;">
        <tr>
            <th>Forma de pago</th>
            <th>Monto</th>
            <th>Fecha</th>
        </tr>
        </thead>
        <tbody>
        @foreach($presupuesto->formas_pago as $formaPago)
            <tr>
                <td align="center">{{ $formaPago->nombre }}</td>
                <td align="right">{{ optional($formaPago->pivot->moneda)->simbolo }} {{ number_format($formaPago->pivot->monto, 2, ",",".") }}</td>
                <td align="right">{{ !is_null($formaPago->pivot->fecha) ? $formaPago->pivot->fecha->format("d/m/Y") : '-----------------' }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot style="background-color: lightgray;">
        <tr>
            <td align="center"><strong>TOTAL</strong></td>
            <td align="right">{{ $presupuesto->monto }}</td>
            <td></td>
        </tr>
        </tfoot>
    </table>

    <br>

    <span>Sin otro particular, saludamos a Uds., muy atentamente</span>

    <br><br>

    <table width="100%">
        <tr>
            <td width="100%">
            </td>
            <td width="100%">
            </td>
            <td width="100%">
                <div align="center">
                    <strong>{{ $presupuesto->usuario->full_name }}</strong>
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%"></td>
            <td width="100%"></td>
            <td width="100%">
                <div align="center">
                    Correo: <a href="mailto:{{ $presupuesto->usuario->email }}">{{ $presupuesto->usuario->email }}</a>
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%"></td>
            <td width="100%"></td>
            <td width="100%">
                <div align="center">
                    <a href="https://www.robertochelala.com.ar/">www.robertochelala.com.ar</a>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 810, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>
