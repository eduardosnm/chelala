@extends('layouts.master')
@section('titulo', 'Listado de presupuestos')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <div class="row mb-4">
        <div class="col">
            <a href="{{ route('presupuestos.crear') }}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Nuevo presupuesto</span>
            </a>

        </div>
        <div class="col-md-3">
            <form action="{{ route('presupuestos.listado') }}" method="GET" id="buscarPresupuestoForm">
                <div class="input-group">
                    <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" id="buscarPresupuesto"
                           placeholder="Buscar cliente por razón social o cuit">
                    <div class="input-group-append">
                        <a class="btn btn-primary" href="{{ route('clientes.listado') }}" title="Limpiar resultados">
                            <i class="fas fa-broom"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <th>Nro</th>
            <th>Cliente</th>
            <th>Producto</th>

            <th>Importe</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @forelse($presupuestos as $presupuesto)
                <tr>
                    <td>{{ $presupuesto->id }}</td>
                    <td>{{ $presupuesto->cliente->razon_social }}</td>
                    <td>{{ $presupuesto->productos_asociados }}</td>
                    <td>{{ number_format($presupuesto->monto, 2, ",", ".")}} </td>
                    <td>
                        <a href="{{ route('presupuestos.editar', $presupuesto) }}" class="btn btn-info btn-circle btn-sm editar cursor" title="Información">
                            <i class="fas fa-edit"></i>
                        </a>

                        <a href="{{ route('presupuestos.ver', $presupuesto) }}" class="btn btn-success btn-circle btn-sm editar cursor" title="Reenviar">
                            <i class="fas fa-envelope"></i>
                        </a>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        {{ $presupuestos->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection

