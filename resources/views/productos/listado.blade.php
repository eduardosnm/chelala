@extends('layouts.master')
@section('titulo', 'Listado de productos')
@section('contenido')
    <style>
        img {
            display: block;
            max-width:320px;
            max-height:180px;
            width: auto;
            height: auto;
        }
    </style>
    @include('partials.errors')
    @include('partials.success')
    <div class="row mb-4">
        <div class="col">
            <button class="btn btn-primary btn-icon-split"
                    data-toggle="modal" data-target="#crearProducto">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                <span class="text">Nuevo producto</span>
            </button>

        </div>
        <div class="col-md-3">
            <form action="{{ route('productos.listado') }}" method="GET" id="buscarProductoForm">
                <div class="input-group">
                    <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" id="buscarProducto"
                           placeholder="Buscar prod por nombre, modelo o marca">
                    <div class="input-group-append">
                        <a class="btn btn-primary" href="{{ route('productos.listado') }}" title="Limpiar resultados">
                            <i class="fas fa-broom"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <th>Nombre</th>
            <th>Modelo</th>
            <th>Descripcion</th>
            <th>Marca</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @forelse($productos as $producto)
                <tr>
                    <td>{{ $producto->nombre }}</td>
                    <td>{{ $producto->modelo }}</td>
                    <td>{{ strip_tags($producto->desc_excerpt) }}</td>
                    <td>{{ $producto->marca->nombre }} </td>
                    <td>
                        <a href="{{ route('productos.editar', $producto) }}" class="btn btn-warning btn-circle btn-sm editar cursor" title="Editar">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade bd-example-modal-lg" id="crearProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Crear producto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="crearProductoForm" action="{{ route('productos.guardar') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">
                                @include('productos.partials.form-crear', ["marcas" => $marcas])
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn_submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="crearMarcaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Crear marca</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="nombreMarca">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id="guardarMarca" disabled class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        {{ $productos->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function () {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagenProducto').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.custom-file-input').on('change',function(e){
                let fileName = $("#imagen").get(0).files[0].name;
                let nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
                if(this.files[0].size > 2097152){
                    alert("¡La imagen es demasiado pesada! Seleccione una imagen menor a 1MB.");
                    this.value = "";

                }
                readURL(this);
            });

            $("#btn_submit").on("click", function (e) {
                e.preventDefault();
                console.log("entra");
                $("#crearProductoForm").submit();
            });

            $('#crearMarca').on("click", function(e){
                e.preventDefault();
                console.log("entra")
                $('#crearProducto').modal('hide');
                $('#crearMarcaModal').modal('show');
            });

            $("#nombreMarca").on("keyup", function (e) {
                e.preventDefault();
                if ($(this).val() != '') {
                    $("#guardarMarca").prop("disabled", false);
                }else {
                    $("#guardarMarca").prop("disabled", true);
                }
            });

            $("#guardarMarca").on("click", function (e) {
                e.preventDefault();
                let nombre = $("#nombreMarca").val();
                let url = "{{ route('marcas.guardar') }}";

                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: "json",
                    data: {
                        nombre: nombre
                    }
                }).done(function (result) {
                    $("#marca_id").append('<option value="'+result.id+'">'+result.nombre+'</option>');
                    $('#crearMarcaModal').modal('hide');
                    $('#crearProducto').modal('show');
                }).fail(function (result) {
                    console.log(result);
                })
            });
        });

    </script>
@endsection
