@extends('layouts.master')
@section('titulo', 'Editar producto')
@section('contenido')
    <style>
        img {
            display: block;
            max-width:320px;
            max-height:180px;
            width: auto;
            height: auto;
        }
    </style>
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            @include('partials.success')
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary text-capitalize">{{ $producto->nombre }} {{ $producto->modelo }}</h6>
                </div>
                <div class="card-body">
                    <form id="editarProductoForm" action="{{ route('productos.actualizar', $producto) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="nombre" name="nombre" placeholder="Nombre" value="{{ $producto->nombre }}">
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="modelo" name="modelo" placeholder="Modelo" value="{{ $producto->modelo }}">
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                <div class="form-group">
                                    <select name="marca_id" id="marca_id" required class="form-control">
                                        <option selected disabled>Seleccione la marca</option>
                                        @foreach($marcas as $marca)
                                            <option value="{{ $marca->id }}" @if($marca->id == $producto->marca->id) selected @endif>{{ ucfirst($marca->nombre) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" name="imagen" accept=".png, .jpg, .jpeg" class="custom-file-input" id="imagen">
                                        <label class="custom-file-label" for="imagen" aria-describedby="inputGroupFileAddon02">{{ !is_null($producto->imagen_id) ? $producto->imagen->nombre : "Seleccionar imagen" }}</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="inputGroupFileAddon02"><i class="fas fa-cloud-upload-alt"></i> Subir</span>
                                    </div>
                                </div>

                                @if(!is_null($producto->imagen_id))
                                    <img id="imagenProducto" src="{{ asset("images/productos/".$producto->imagen->nombre) }}" class="img-fluid img-thumbnail">
                                @else
                                    <img id="imagenProducto" src="" alt="">
                                @endif

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <textarea name="descripcion" required id="descripcion" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="form-row mt-3">
                            <div class="col-12 text-center">
                                <button type="submit" id="enviar" class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('tinymce/tinymce.js') }}"></script>
    <script>
        $(function () {
            let descripcion = @json($producto->descripcion);
            tinymce.init({
                selector: '#descripcion',
                language: 'es',
                init_instance_callback: function(editor) {
                    editor.setContent(descripcion, {format: 'html'});
                }
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#imagenProducto').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('.custom-file-input').on('change',function(e){
                let fileName = $("#imagen").get(0).files[0].name;
                let nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
                readURL(this);
            });

            $('#enviar').click(function (e) {
                e.preventDefault();
                $("#editarProductoForm").submit();
            })
        })
    </script>
@endsection
