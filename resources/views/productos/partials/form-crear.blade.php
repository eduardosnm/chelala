<div class="form-row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="form-group">
            <input type="text" class="form-control" required id="nombre" name="nombre" placeholder="Nombre">
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="form-group">
            <input type="text" class="form-control" required id="modelo" name="modelo" placeholder="Modelo">
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="form-group">
            <select name="marca_id" id="marca_id" required class="form-control">
                <option selected disabled>Seleccione la marca</option>
                @foreach($marcas as $marca)
                    <option value="{{ $marca->id }}">{{ ucfirst($marca->nombre) }}</option>
                @endforeach
            </select>
            <button class="btn btn-info btn-sm float-right my-2" id="crearMarca">Nueva marca</button>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="input-group mb-3">
            <div class="custom-file">
                <input type="file" name="imagen" accept=".png, .jpg, .jpeg" class="custom-file-input" id="imagen">
                <label class="custom-file-label" for="imagen" aria-describedby="inputGroupFileAddon02">Seleccionar imagen</label>
            </div>
            <div class="input-group-append">
                <span class="input-group-text" id="inputGroupFileAddon02"><i class="fas fa-cloud-upload-alt"></i> Subir</span>
            </div>
        </div>

        <img id="imagenProducto" src="" alt="">
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
        <div class="form-group">
            <textarea name="descripcion" required class="form-control" cols="30" rows="10"></textarea>
        </div>
    </div>
</div>

