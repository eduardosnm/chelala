<div class="form-row">
    <div class="col-6">
        <div class="form-group">
            <input type="text" class="form-control" required id="razon_social" name="razon_social" placeholder="Razon social o nombre">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-6">
        <div class="form-group">
            <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
        </div>
    </div>
    <div class="col-6">

        <div class="form-group">
            <input type="text" class="form-control" pattern="([0-9]{11}|[0-9]{2}-[0-9]{8}-[0-9]{1})" id="cuit" name="cuit"
                   placeholder="CUIT solo números">
        </div>
    </div>
</div>
