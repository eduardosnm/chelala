@extends('layouts.master')
@section('titulo', 'Editar cliente')
@section('contenido')
    <div class="row">
        <div class="col-12">
            @include('partials.errors')
            @include('partials.success')
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary text-capitalize">{{ $cliente->razon_social }}</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('clientes.actualizar', $cliente) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="razon_social" name="razon_social" placeholder="Razon social"
                                    value="{{ $cliente->razon_social }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                    value="{{ $cliente->email }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono"
                                    value="{{ $cliente->telefono }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="cuit" name="cuit" placeholder="CUIT"
                                    value="{{ $cliente->cuit }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="estado" id="defaultCheck1" @if($cliente->estado) checked @endif>
                                    <label class="form-check-label" for="defaultCheck1">
                                        Habilitado
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-12 text-center">
                                <button class="btn btn-primary btn-block">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(function(){
            $("#cuit").mask("00-00000000-0");
        })
    </script>
@endsection
