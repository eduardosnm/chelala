@extends('layouts.master')
@section('titulo', 'Listado de clientes')
@section('contenido')

    @include('partials.errors')
    @include('partials.success')
    <div class="row mb-4">
        <div class="col">
            <button class="btn btn-primary btn-icon-split"
                    data-toggle="modal" data-target="#crearUsuario">
                    <span class="icon text-white-50">
                        <i class="fas fa-user-plus"></i>
                    </span>
                <span class="text">Nuevo cliente</span>
            </button>

        </div>
        <div class="col-md-3">
            <form action="{{ route('clientes.listado') }}" method="GET" id="buscarClienteForm">
                <div class="input-group">
                    <input type="text" class="form-control" value="{{ request('buscar') }}" name="buscar" id="buscarCliente"
                           placeholder="Buscar cliente por razón social o cuit">
                    <div class="input-group-append">
                        <a class="btn btn-primary" href="{{ route('clientes.listado') }}" title="Limpiar resultados">
                            <i class="fas fa-broom"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="thead-dark">
            <th>Razon social</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>CUIT</th>
            <th>Acción</th>
            </thead>
            <tbody>
            @forelse($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->razon_social }}</td>
                    <td>{{ $cliente->email }}</td>
                    <td>{{ $cliente->telefono }}</td>
                    <td>{{ $cliente->cuit }} </td>
                    <td>
                        <a href="{{ route('clientes.editar', $cliente) }}" class="btn btn-warning btn-circle btn-sm editar cursor" title="Editar">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                    </td>
                </tr>
            @empty
                <td colspan="5">No se encontraron resultados</td>
            @endforelse
            </tbody>
        </table>

        <div class="modal fade bd-example-modal-lg" id="crearUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Crear cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="crearUsuarioForm" action="{{ route('clientes.guardar') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="container-fluid">
                                @include('clientes.partials.form-crear')
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{ $clientes->appends([
            'buscar' => request('buscar')
        ])->links() }}
    </div>
@endsection
@section('js')
    <script>
        $(function(){
            $("#cuit").mask("00-00000000-0");
        });
    </script>
@endsection
