/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$ = window.jQuery = require('jquery');
import 'jquery-ui/ui/widgets/autocomplete.js';
require('startbootstrap-sb-admin-2/js/sb-admin-2.min');
window.moment = require('moment');
require("jquery-mask-plugin/dist/jquery.mask.min")


